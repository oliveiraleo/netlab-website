# NetLab's Website

This repository contains the source code from the website [http://netlab.ice.ufjf.br/](http://netlab.ice.ufjf.br/). It uses the [MKDocs](https://github.com/mkdocs/mkdocs) framework and is written using Markdown and HTML

## Installation

This section contains the installation process

On the development machine:

### 1. Build the website

#### 1.1. Clone the source code

```
git clone https://gitlab.com/oliveiraleo/netlab-website.git
```

#### 1.2. Install the requirements

```
cd netlab-website
python -m venv pyvenv
source pyvenv/bin/activate
pip install -r requirements.txt
```

#### 1.3. Build the website files

```
cd netlab-mkdocs
mkdocs build
```

<!-- ## Deployment using Nginx on Debian

TODO: Add the instructions here -->

## Deployment using Apache on FreeBSD

On the server machine:

### 1. Install Apache

```
pkg install apache24
```

**TIP:** Depending on one's OS setup, it may be required to run this command as root

#### 1.1. Enable the service at startup

```
vi /etc/rc.conf
```

Add the following line at its end:

```
apache2_enable="YES"
```

#### 1.2. Update the http config

Edit the file /usr/local/etc/apache24/httpd.conf and change `ServerName` to "localhost:80" (without quotes)

### 2. Delete the default index.html file

```
cd /usr/local/www/apache24/data/
rm index.html
```

### 3. Update data folder permissions

**TIP:** For jails, this step might be skipped

```
cd /usr/local/www/apache24/data/
chown user:group .
```

**NOTE:** Adapt the `user` and `group` to your local environment

### 4. Deploy the website

#### 4.1. Copy the source files to the server

Use scp (or rsync) to copy the data inside "site" folder to the path below

```
/usr/local/www/apache24/data/
```

#### 4.2- Restart the service to apply

```
service apache24 restart
```

## Update

To update the website, run a `git pull` on the development machine and execute the steps [1.3](./README.md#13-build-the-website-files) and [4](./README.md#4-deploy-the-website) from the [Installation](./README.md#installation) section

## License

This project is licensed under the [GNU GPLv3 license](./LICENSE)
