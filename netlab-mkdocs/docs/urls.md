# Useful URLs

## UFJF

- [Federal University of Juiz de Fora main website](https://ufjf.br)

## PPGCC

- [Postgraduate program in Computer Science main website](https://www2.ufjf.br/pgcc)
- [Other research laboratories from PPGCC](https://www2.ufjf.br/pgcc/laboratorios-de-pesquisa/)