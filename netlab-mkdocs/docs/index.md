# Home Page

The Networks and Distributed Systems Laboratory (NetLab) has been dedicated to research in Computer Networks and Distributed Systems fields since its establishment in 2010. Research initiatives have been carried out, with collaboration and knowledge-sharing facilitated across national and international boundaries.

NetLab is affiliated with the [Computer Science Postgraduate Program (PPGCC)](https://www2.ufjf.br/pgcc/) of the [Federal University of Juiz de Fora](https://www2.ufjf.br/) (UFJF). [Its headquarters](https://www.google.com/maps/place/Programa+de+Pós-Graduação+em+Ciência+da+Computação+-+PGCC%2FUFJF/@-21.7768137,-43.3723246,19z/data=!3m1!4b1!4m6!3m5!1s0x989ba19f8f42db:0x832c36d262fe8a04!8m2!3d-21.7768149!4d-43.3716809!16s%2Fg%2F11gdkcvw5h) are situated on the UFJF's [Juiz de Fora campus](https://www.google.com/maps/place/Universidade+Federal+de+Juiz+de+Fora/@-21.7749021,-43.3705976,16z/data=!4m6!3m5!1s0x989b9f99b33491:0xd9b48a8f80341c0d!8m2!3d-21.7732767!4d-43.368996!16zL20vMDV2MF9i), located in [Juiz de Fora](https://www.google.com/maps/place/Juiz+de+Fora,+MG/@-21.7290588,-43.382521,12z/data=!3m1!4b1!4m6!3m5!1s0x989c43e1f85da1:0x6236b026b3a0a468!8m2!3d-21.7623932!4d-43.3434669!16zL20vMDNsZzRm), Minas Gerais, Brazil. Explore other PPGCC-affiliated laboratories [on this page](https://www2.ufjf.br/pgcc/laboratorios-de-pesquisa/).

## Research Topics

Current research focus areas at NetLab include:

- Computer Networks
- Blockchain
- Complex Networks
- Cybersecurity

## External references

### CNPQ ([?](https://en.wikipedia.org/wiki/National_Council_for_Scientific_and_Technological_Development))

Access our group's listing in CNPq's Directory [on this URL](https://dgp.cnpq.br/dgp/espelhogrupo/2571360156506724)

### GitHub

Check out our GitHub organization [on this page](https://github.com/netlabufjf)

<p align="center" style="font-size:16px">Website content last updated on September, 2024</p>
