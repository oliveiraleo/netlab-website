# OpenFlow Data Planes Performance Evaluation

On this page it's possible to download the data used in the following papers:

## OpenFlow Data Planes Performance Evaluation

Leonardo C. Costa, Alex B. Vieira, Erik de Britto e Silva, Daniel F. Macedo, Luiz F. M. Vieira, Marcos A. M. Vieira, Manoel da Rocha Miranda Junior, Gabriel Fanelli Batista, Augusto Henrique Polizer, André Vinícius Gomes Santos Gonçalves, Geraldo Gomes, Luiz H. A. Correia

### Paper

IFIP/IEEE International Symposium on Integrated Network Management, IEEE IM 2017 >> [Full Document](https://homepages.dcc.ufmg.br/~mmvieira/cc/papers/IFIPIEEEIntegrated_Management_2017.pdf)

### Bibtex

```
@inproceedings{Costa_IFIP_2017,
    address={Lisbon}, 
    title={Performance evaluation of OpenFlow data planes}, 
    ISBN={978-3-901882-89-0}, 
    url={https://ieeexplore.ieee.org/document/7987314/}, 
    DOI={10.23919/INM.2017.7987314}, 
    booktitle={2017 IFIP/IEEE Symposium on Integrated Network and Service Management (IM)}, 
    publisher={IEEE}, 
    author={Costa, Leonardo C. and Vieira, Alex B. and De Britto E Silva, Erik and Macedo, Daniel F. and Gomes, Geraldo and Correia, Luiz H.A. and Vieira, Luiz F.M.}, year={2017}, 
    month={05}, 
    pages={470–475}
}
```

### Scripts and Traces

Google Drive URL: TDB

---

## Does OpenFlow Really Decouple the Data Plane from the Control Plane?

Thiago Moratori; Daniel Fernandes Macedo; Michele Nogueira; Alex Borges Vieira

IEEE Symposium on Computers and Communications, ISCC 2018 >> [Full Document](https://www.researchgate.net/profile/Alex-Borges/publication/329064478_Does_OpenFlow_Really_Decouple_The_Data_Plane_from_The_Control_Plane/links/5cd9bc7e92851c4eab9d61ec/Does-OpenFlow-Really-Decouple-The-Data-Plane-from-The-Control-Plane.pdf)

### Bibtex

```
@inproceedings{Peixoto_Vieira_Nogueira_Macedo_2018, 
    address={Natal},
    title={Does OpenFlow Really Decouple The Data Plane from The Control Plane?}, 
    ISBN={978-1-5386-6950-1}, 
    url={https://ieeexplore.ieee.org/document/8538627/}, 
    DOI={10.1109/ISCC.2018.8538627}, 
    booktitle={2018 IEEE Symposium on Computers and Communications (ISCC)}, 
    publisher={IEEE}, 
    author={Peixoto, Thiago M. and Vieira, Alex B. and Nogueira, Michele and Macedo, Daniel F.}, 
    year={2018}, 
    month={06}, 
    pages={00596–00601}
}
```

### Scripts and Traces

TBD