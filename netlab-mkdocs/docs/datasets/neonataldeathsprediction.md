# Neonatal Death Prediction

On this page it's possible to download the data used in the following papers:

## Neonatal Risk Classification Using Machine Learning and Data from Public Health Information Systems and Brazilian Population Census

### Bibtex

Paper under review (Artificial Intelligence in Medicine Journal)

### Dataset

| Name | File Size | Lines |
|---|---|---|
| neonataldeathdata.tar.gz | 69 MB | 8.288.404 |

### Supplementary Material

[Figures and Tables](../assets/files/neonataldeathsprediction/full_supplmaterial.pdf)

## Predição de Óbito Neonatal usando Dados dos Sistemas de Informação do SUS e de Censo Demográfico

Jorge R. H. Moreira, Heder S. Bernardino, Alex B. Vieira

### Bibtex

```
@inproceedings{Moreira_Bernardino_Vieira_2022, 
    address={Porto Alegre, RS, Brasil},
    title={Predição de Óbito Neonatal usando Dados dos Sistemas de Informação do SUS e de Censo Demográfico}, 
    url={https://sol.sbc.org.br/index.php/sbcas/article/view/21635},
    DOI={10.5753/sbcas.2022.222569}, 
    booktitle={Anais do XXII Simpósio Brasileiro de Computação Aplicada à Saúde (SBCAS 2022)}, 
    publisher={Sociedade Brasileira de Computação - SBC}, 
    author={Moreira, Jorge R. H. and Bernardino, Heder S. and Vieira, Alex B.}, 
    year={2022}, 
    month={06},
    pages={234–245},
    location = {Teresina}
}
```

### Scripts and Traces

[Supplementary Material](../assets/files/neonataldeathsprediction/supplmaterial.pdf)