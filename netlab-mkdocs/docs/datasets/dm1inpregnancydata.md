# Modelos de Aprendizado de Máquina na Predição de DM1 na Gestação usando Dados do SUS

On this page it's possible to download the data used in the following paper:

## Bibtex

```
@inproceedings{moreira_sbcas2021,
    author = {Jorge Moreira and Heder Bernardino and Helio Barbosa and Alex Vieira},
    title = {Modelos de Aprendizado de Máquina na Predição de Diabetes Tipo 1 na Gestação usando Dados do Sistema Único de Saúde},
    booktitle = {Anais do XXI Simpósio Brasileiro de Computação Aplicada à Saúde},
    location = {Evento Online},
    year = {2021},
    keywords = {},
    issn = {2763-8952},
    pages = {392–403},
    publisher = {SBC},
    address = {Porto Alegre, RS, Brasil},
    doi = {10.5753/sbcas.2021.16082},
    url = {https://sol.sbc.org.br/index.php/sbcas/article/view/16082}
}
```

## Dataset

| Name | File Size | Lines |
|---|---|---|
| [dm1pregnancydata.tar.gz](https://drive.google.com/drive/folders/10DN4RjWp0n-pV2uwzm01fYfdoKUcGBDz) | 3.73 GB | 119.692.608 |