# Visible Light Communication

**Visible Light Communication – Let there be light!**

**Let’s talk about the electromagnetic spectrum**

As you can see in the image below, the electromagnetic spectrum is huge.

**LEDs – A new hope to Visible Light Communication**

## VLC First Attempt – Using two Arduinos

Firstly, we tried using two Arduino boards to send and receive light, using off-the-shelf components, such as LEDs. It is very important to notice that this first experiment does not intend to create something new. The code used was created by professor Jonathan Piat, which can be found at [https://github.com/jpiat/arduino](https://github.com/jpiat/arduino), and it implements a very simple VLC emitter and receiver. The components used for this first test are:

- 2 Green LEDs;
- LCD Display;
- A 1Mohm resistor;
- Potentiometer (for the LCD display).

Instead of visualizing the income data only through the Arduino’s serial monitor, we added a simple lcd display at the receiver’s side, which will update the data received from time to time.

The video below shows our first attempt to send some data using light. As it can be seen in the video, there are two LEDs, the emitter side and the transmitter side. When the Arduinos are powered on, and the light source is put in front of the receiver, the LCD display will show the data (in this particular case, the data is a string with value “Primeiro teste”, which means first test, in English).  For now, the video is in Portuguese, but soon we will make an English one.

<iframe
    width="100%"
    height="600"
    src="https://www.youtube.com/embed/T8sewmxX9qM"
    frameborder="0"
    allow="encrypted-media"
    allowfullscreen
>
</iframe>

## Light and its obstacles

In the next video, we try to show how obstacles can interfere with our communication. For that, we put emitter and receiver face to face, in a way that data will be sent continuously. The emitter is counting from 0 to 9, and sending each number via light. Then, the receiver gets the data and displays it in the LCD display. If we insert an obstacle between emitter and receiver, the data will not get to the receiver, as supposed. This is a simple way to show one of the greatest challenges for VLC to emerge as a commercializable technology.

<iframe
    width="100%"
    height="600"
    src="https://www.youtube.com/embed/2RodohnPQdA"
    frameborder="0"
    allow="encrypted-media"
    allowfullscreen
>
</iframe>
