# Car-Sharing Datasets

On this page it's possible to download the data used in the following papers:

Aquiles, V. ; Silva, F. R. ; Cocca, M. ; Vassio, L. ; Almeida, Jussara ; Vieira, Alex . Characterizing client usage patterns and service demand for car-sharing systems. INFORMATION SYSTEMS, v. 1, p. 101448, 2019.

## Bibtex

```
@article{ALENCAR2019101448,
title = “Characterizing client usage patterns and service demand for car-sharing systems”,
author = “Victor A. Alencar and Felipe Rooke and Michele Cocca and Luca Vassio and Jussara Almeida and Alex Borges Vieira”,
journal = “Information Systems”,
pages = “101448”,
year = “2019”,
issn = “0306-4379”,
doi = “https://doi.org/10.1016/j.is.2019.101448”,
url = “http://www.sciencedirect.com/science/article/pii/S0306437919305009”,
}
```

Rooke F., Aquiles V., Vieira A.B., Almeida J.M., Drago I. (2019) Characterizing Usage Patterns and Service Demand of a Two-Way Car-Sharing System. In: Oliveira J., Farias C., Pacitti E., Fortino G. (eds) Big Social Data and Urban Computing. BiDU 2018. Communications in Computer and Information Science, vol 926. Springer, Cham

## Bibtex

```
@inbook{Rooke_Aquiles_Vieira_Almeida_Drago_2019, 
    address={Cham}, 
    series={Communications in Computer and Information Science}, 
    title={Characterizing Usage Patterns and Service Demand of a Two-Way Car-Sharing System}, 
    volume={926},
    ISBN={978-3-030-11237-0}, 
    url={http://link.springer.com/10.1007/978-3-030-11238-7_1}, 
    DOI={10.1007/978-3-030-11238-7_1}, 
    booktitle={Big Social Data and Urban Computing}, 
    publisher={Springer International Publishing}, 
    author={Rooke, Felipe and Aquiles, Victor and Vieira, Alex Borges and Almeida, Jussara M. and Drago, Idilio}, 
    editor={Oliveira, Jonice and Farias, Claudio M. and Pacitti, Esther and Fortino, Giancarlo}, 
    year={2019}, 
    pages={3–17}, 
    collection={Communications in Computer and Information Science}
}
```

## Scripts

[Modo Scripts](https://github.com/netlabufjf/Modo-Scripts)

[Evo Scripts](https://github.com/netlabufjf/Evo-Scripts)

[Car2Go Scripts](https://github.com/netlabufjf/Car2go-Scripts)

## Modo Traces

| Name | File Size | Lines |
|---|---|---|
| Modo raw data | 425 MB | 64.361.653 |
| Modo travels | 1.2 MB | 98.916 |

[Modo field description](../assets/files/carsharingdata/modo_field_description.html)

## Evo Traces

| Name | File Size | Lines |
|---|---|---|
| Evo raw data | 4.9 GB | 355.646.508 |
| Evo travels | 29 MB | 644.512 |

[Evo field description](../assets/files/carsharingdata/evo_field_description.html)

## Car2Go Traces

| Name | File Size | Lines |
|---|---|---|
| Car2Go travels | 26 MB | 1.246.406 |

[Car2Go field description](../assets/files/carsharingdata/car2go_field_description.html)
