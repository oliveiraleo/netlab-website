# CoVeC: Coarse-Grained Vertex Clustering for Efficient Community Detection in Sparse Complex Networks

On this page it's possible to download the data used in the following paper:

Gustavo Carnivali, Paulo Esquef, Alex Borges Vieira, Artur Ziviani

## Bibtex

```
@article{Carnivali_Vieira_Ziviani_Esquef_2020,
    title={CoVeC: Coarse-grained vertex clustering for efficient community detection in sparse complex networks}, 
    volume={522}, 
    ISSN={00200255}, 
    DOI={10.1016/j.ins.2020.03.004}, 
    journal={Information Sciences}, 
    author={Carnivali, Gustavo S. and Vieira, Alex B. and Ziviani, Artur and Esquef, Paulo A.A.}, 
    year={2020}, 
    month={06},
    pages={180–192}
}
```

## Scripts and Source Codes

TBD

## Traces and Datasets

TBD 