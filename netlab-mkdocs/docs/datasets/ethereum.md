# Ethereum

On this page it's possible to download the data used in the following papers:

## Analysis of account behaviors in Ethereum during an economic impact event

### Bibtex

```
@article{Oliveira_Rezende_Bernardino_Villela_Vieira_2022, 
    title={Analysis of account behaviors in Ethereum during an economic impact event},
    url={https://arxiv.org/abs/2206.11846}, 
    DOI={10.48550/ARXIV.2206.11846}, 
    publisher={arXiv}, 
    author={Oliveira, Pedro Henrique F. S. and Rezende, Daniel Muller and Bernardino, Heder Soares and Villela, Saulo Moraes and Vieira, Alex Borges}, 
    year={2022}
}
```

### Datasets

| Name | File Size | Lines |
|---|---|---|
| [ethereum-Flashbots-datasets](https://drive.google.com/drive/u/2/folders/1rmvKk3wwKv0q03-Qz77Zsbf_u5jWTJx2) | 161.5 MB | TBD |
| [ethereum-mainnet-datasets](https://drive.google.com/drive/u/2/folders/1rmvKk3wwKv0q03-Qz77Zsbf_u5jWTJx2) | 6.39 GB | TBD |

## Mapping User Behaviors to Identify Professional Accounts in Ethereum Using Semi-Supervised Learning

### Bibtex

```
@article{Valadares_Villela_Bernardino_Gonçalves_Vieira_2023, 
    title={Mapping user behaviors to identify professional accounts in Ethereum using semi-supervised learning}, 
    volume={229}, 
    ISSN={09574174}, 
    DOI={10.1016/j.eswa.2023.120438}, 
    journal={Expert Systems with Applications}, 
    author={Valadares, Júlia Almeida and Villela, Saulo Moraes and Bernardino, Heder Soares and Gonçalves, Glauber Dias and Vieira, Alex Borges}, 
    year={2023}, 
    month={11}, 
    pages={120438}
}
```

### Datasets

| Name | File Size | Lines |
|---|---|---|
| [Semi supervised results](https://drive.google.com/file/d/1A538A2miRb8AFQx6LLFxgiD1V39vz3jc/view) | 4.3 KB | 303 |
| [Dataset labeled](https://drive.google.com/file/d/1fXfzWHZc9Oq1O5DgIiLrLYRUxJMH5MP4/view) | 1.4 MB | 17.021 |

### Source code

[GitHub](https://github.com/juliaavaladares/data-science-ethereum)