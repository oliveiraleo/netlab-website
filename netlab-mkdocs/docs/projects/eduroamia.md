# EduroamIA

## EduroamIA: Inteligência Artificial Aplicada à Previsão de Eventos de Autenticação Federada no eduroam

### Resumo

Este projeto visa a predição de eventos críticos no serviço eduroam, alertando seu administrador em nível da federação, e também em nível institucional, sobre anomalias relacionadas à autenticação de usuários. Com a utilização de técnicas de inteligência artificial e baseando-se na análise de grandes massas de dados de registros do serviço, tanto em modo offline quanto online. Algoritmos de aprendizado de máquina são aplicados a fim de identificar problemas relacionados à autenticação de usuários de instituições específicas e possíveis comportamentos anormais correlacionados à autenticação federada. Para alcançar este objetivo são utilizados como entrada os registros do serviço RADIUS no nível da federação eduroam de usuários em roaming.

### Motivação

Desde o início do projeto eduroam, mais e mais instituições têm aderido a esta federação de acesso seguro à rede sem fio acadêmica em nível global. Atualmente o eduroam já está em 101 países e já realizou mais de 1 bilhão de autenticações. Com este volume de clientes fica claro que, além dos dados gerados pelas tentativas de acesso/autenticação, a saúde do serviço de roaming federado deve ser prioridade. Neste momento propomos uma forma de empregar tecnologias que possam identificar possíveis anomalias no serviço de autenticação, antes mesmo que o próprio administrador as perceba. Isso deve ser realizado através do monitoramento em tempo real dos registros de autenticação no nível da federação do servidor RADIUS. É importante também destacar que a RNP atualmente é responsável pelo servidor raíz da América Latina, possuindo grande massa de dados de autenticação das instituições parceiras eduroam e o roaming de seus usuários 1. O serviço eduroam é um dos serviços no catálogo de Gestão de Identidade da RNP.

Em relação ao tratamento e análise de registros, tem-se que grande parte da enorme quantidade de dados atualmente são geradas por sistemas automatizados, e o aproveitamento dessa informação como forma de se criar valor a um serviço/negócio é fundamental. O volume atual de dados gerados diariamente somente por um servidor eduroam no nível da federação gira em torno de 10GB. Desta forma, iniciativas como a utilização de aprendizado de máquina surge como um método valioso de se obter insights importantes e transformadores desses registros em fontes de dados com valor.

Assim como o RADIUS, a maioria dos produtos de software gera registros/logs que são usados para análise da causa raiz e de uma possível solução de problemas. Embora esses registros ofereçam informações úteis sobre o desempenho em tempo real, é difícil (e muitas vezes inviável) analisá-los manualmente. Seus dados, em geral, não têm estrutura e, muitas vezes, não contêm informações analíticas suficientes. Normalmente, os responsáveis por sua análise têm o objetivo de identificar e alertar se alguma parte do serviço não está funcionando conforme o esperado; procurar por sinais que indicam falhas iminentes em cascata; entender como os ciclos normais de operações mudam durante grandes eventos.

Assim, o monitoramento das operações desses serviços dependem de suas experiências anteriores com o sistema, bem como de certas regras definidas manualmente, para detectar comportamentos anômalos. No entanto, o monitoramento baseado em regras é árduo e não escalável, podendo ainda criar falsos negativos ou acionar alertas sobre anomalias quando não há, criando falsos positivos.

### Proposta

Um caminho razoável e plausível com os recursos computacionais atualmente existentes é, portanto, pensar em desenvolver uma maneira eficiente de detectar anomalias o mais rápido possível. Em vez de confiar apenas no conhecimento do administrador do serviço e definir limites manualmente para detectar comportamentos anômalos, o objetivo deste projeto é desenvolver uma solução tecnológica que aprenda os padrões complexos e responda pela sazonalidade com uma melhor precisão do que os sistemas manuais.

A proposta deste projeto é a utilização de modelos de aprendizado de máquina disponíveis pela Microsoft Azure para análise e predição de eventos 2 relacionados à autenticação de usuários eduroam. A partir dos registros coletados do servidor RADIUS por uma aplicação de streaming de dados, e.g. o Apache Kafka disponível em HDInsight do Azure e enviado para o 3 pré-processamento, que uma vez realizado deve enviar os dados ao serviço de armazenamento escalável de dados da Azure. A partir dos dados 4 armazenados, estes serão enviados para treinamento e predição de eventos utilizando uma ferramenta como o Azure Monitor , que integra a análise 5 avançada e aprendizado de máquina para monitorar o desempenho do aplicativo de autenticação RADIUS e identifique proativamente problemas, gerando alertas de modo automático.

Além da contribuição sobre a arquitetura de serviço de análise preditiva já descrita, a solução deve ser capaz de identificar problemas relacionados à autenticação do usuário. Avaliando de modo proativo se, por exemplo, uma instituição está rejeitando mais autenticações que um limiar de condições normais, possibilitando indicar uma falha do serviço de autenticação de 6 usuários em roaming daquela origem ou destino.

Durante a submissão desta proposta foi analisada a base de dados que será utilizada neste projeto. Ela é caracterizada por possuir um endereço MAC do usuário, sua instituição, e o horário que foi realizada a tentativa de autenticação. Sendo assim, temos a confiança de que a utilização da base atual existente de logs do RADIUS é passível de pré-processamento e análise por modelos de predição.

![Diagrama Fase 1](../assets/img/projects/eduroamia/Diagrama-fase-1.png "Diagrama Fase 1")

![Diagrama Fase 2](../assets/img/projects/eduroamia/Diagrama-fase-2.png "Diagrama Fase 2")

### Escopo

Este projeto se divide em duas etapas, ambas com o apoio de soluções de IA da Microsoft e utilizando dados reais do serviço eduroam na RNP. Uma primeira etapa trata os dados de maneira offline, realizando um treinamento de modelos de predição e a avaliação do impacto dos parâmetros de configuração das funções estatísticas utilizadas. Nesta fase são validados e avaliados os logs do serviço RADIUS de autenticação, além de definido o escopo do sistema de análise, assim como a escolha do modelo de aprendizado de máquina a ser utilizado.

No fim desta fase é realizada a segunda etapa, responsável pela análise online dos registros do RADIUS. A solução deverá ser capaz de identificar falhas anômalas de autenticação de usuários em roaming e de comportamento do serviço, como por exemplo, um perfil de rejeições de autenticações fora do limiar dito pelo modelo como normal para aquela instituição.

### Interface do Projeto

<iframe src="https://app.powerbi.com/view?r=eyJrIjoiZDE3ZTRkYjMtNDFmNy00NDg1LTk1ZTItMzVhODdmOTM4MjI5IiwidCI6ImUxOWE2ZWZkLTA3MzktNDRmYi04MThiLTBhMDcwZDllMzA3YSJ9" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>

---

<p align="center">
    <a href="https://ufjf.br/"><img src="/assets/img/other-logos/ufjf.png" alt="UFJF Logo" width="20%" height="20%"></a>
    <a href="https://www.rnp.br/"><img src="/assets/img/other-logos/rnp.png" alt="RNP Logo" width="30%" height="20%"></a>
    <a href="https://www.microsoft.com/"><img src="/assets/img/other-logos/Microsoft.png" alt="Microsoft Logo" width="20%" height="20%"></a>
    <img src="/assets/logo/netlab-logo-shadow.png" alt="NetLab Logo" width="10%" height="10%"></a>
</p>