# Contact

Please, contact any of the [heads of the lab](./people.md#heads-of-the-lab)

## Location

### Federal University of Juiz de Fora

José Lourenço Kelmer Street, s/n – Campus Universitário (visit on [Google Maps](https://maps.app.goo.gl/qBpUzuGCytnm7Qzr8))

São Pedro Neighborhood – ZIP Code: 36036-900 – Juiz de Fora – MG

### Postgraduate program in Computer Science

Instituto de Ciências Exatas (ICE) – Room 3405 – Phone: (32) 2102-3387 Ramal 20 (visit on [Google Maps](https://maps.app.goo.gl/ch1oPNE5UH6t1e9N8))