# People

Members of the lab

## Professors

### Heads of the lab

<table>
    <tr>
        <th style="text-align:center">Dr. Alex Borges Vieira</th>
        <th style="text-align:center">Dr. Edelberto Franco Silva </th>
    </tr>
    <tr>
        <td style="text-align:center"><a href="https://www2.ufjf.br/pgcc/pessoas/corpo-docente/#alex-borges-vieira"><img src="https://servicosweb.cnpq.br/wspessoa/servletrecuperafoto?tipo=1&id=K4733904E3" alt="Profile picture from Dr. Alex Vieira" width="250px" height="250px"> </a> </td>
        <td style="text-align:center"><a href="https://www2.ufjf.br/pgcc/pessoas/corpo-docente/#edelberto-franco-silva">
<img src="https://servicosweb.cnpq.br/wspessoa/servletrecuperafoto?tipo=1&id=K4744920Z9" alt="Profile picture from Dr. Edelberto Silva" width="250px" height="250px"> </a> </td>
    </tr>
    <tr>
        <td style="text-align:center">
            <a href="http://lattes.cnpq.br/9037224811267705">Lattes CV</a>, <br>
            <a href="https://orcid.org/0000-0003-0821-126X">ORCID</a>, <br>
            <a href="https://scholar.google.com.br/citations?user=tjvh6lkAAAAJ">Google Scholar</a>
        </td>
        <td style="text-align:center">
            <a href="https://sites.google.com/a/ice.ufjf.br/edelbertofranco/">Personal Website</a>, <br>
            <a href="http://lattes.cnpq.br/3987091765361506">Lattes CV</a>, <br>
            <a href="https://orcid.org/0000-0002-0058-9260">ORCID</a>, <br>
            <a href="https://scholar.google.com/citations?user=8fURLscAAAAJ">Google Scholar</a>
        </td>
    </tr>
</table>


## Students

### PhD

- [Airton Ribeiro de Moura G. Filho](http://lattes.cnpq.br/4414087558621755)

### MSc

- [Luciano Fernandes da Rocha](http://lattes.cnpq.br/5610162289114413)
- [Rodrigo Funchal Oliveira](http://lattes.cnpq.br/5901671364165732)
- [Leonardo Azalim de Oliveira](http://lattes.cnpq.br/7749238753232176)
- [Khalid Usman](http://lattes.cnpq.br/5424169364762564)
- [Júlia Almeida Valadares](http://lattes.cnpq.br/9584855399190722)

### BSc

- [Antônio Marcos Souza Pereira](http://lattes.cnpq.br/0111791927770132)
- [Rafael Fialho Pinto Coelho](http://lattes.cnpq.br/4729325212728082)
- [Antonio Marcos da Silva Junior](http://lattes.cnpq.br/4842450356777549)
- [Mariana Siano Pinto](TODO)

### *Alumini*

- Breno Almeida dos Santos Rodrigues Machado
- José Eduardo de Azevedo Sousa
- [Roberto Massi de Oliveira](http://lattes.cnpq.br/4554515788535957) (Postdoc)
- Airton Ribeiro de Moura G. Filho (MSc)
- [Bruno Marques Cremonezi](http://lattes.cnpq.br/3800140703764731) (PhD)
- [Genilson Israel da Silva](http://lattes.cnpq.br/1752310502581604) (MSc)
- [Mayara Amanda da Silva](http://lattes.cnpq.br/6672884846631832) (MSc)
- [Lucas Rodrigues Frank](http://lattes.cnpq.br/2924098385985246) (MSc)
- [Rodrigo Torres Rego](http://lattes.cnpq.br/6714115345867381) (BSc)
- [Frederico de Oliveira Sales](http://lattes.cnpq.br/6769748106242747) (MSc)